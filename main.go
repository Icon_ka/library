package main

import (
    _ "github.com/golang-migrate/migrate/v4/source/file"
    _ "github.com/lib/pq"
    "library/run"
)

func main() {
    app := run.NewApp()

    app.Bootstrap().Run()
}
