module library

go 1.19

require (
	github.com/Masterminds/squirrel v1.5.4
	github.com/brianvoe/gofakeit/v6 v6.24.0
	github.com/go-chi/chi v1.5.5
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.2
	go.uber.org/zap v1.26.0
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/opencontainers/image-spec v1.1.0-rc2.0.20221005185240-3a7f492d3f1b // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/tools v0.10.0 // indirect
)
