package author

import (
    "database/sql"
    "library/internal/entities"
    "context"
    "library/internal/repository"
)

type AuthorService struct {
    r repository.Repository
}

func NewAuthorService(db *sql.DB) *AuthorService {
    return &AuthorService{
        r: *repository.NewRepository(db),
    }
}

func (a AuthorService) Create(ctx context.Context, author entities.Author) error {
    return a.r.Author.Create(ctx, author)
}

func (a AuthorService) GetByID(ctx context.Context, id int) (entities.Author, error) {
    auth, err := a.r.Author.GetById(ctx, id)
    if err != nil {
        return entities.Author{}, err
    }
    return auth, nil
}

func (a AuthorService) Exists(ctx context.Context, id int) (bool, error) {
    return a.r.Author.Exists(ctx, id)
}

func (a AuthorService) List(ctx context.Context) ([]entities.Author, error) {
    auths, err := a.r.Author.List(ctx)

    if err != nil {
        return []entities.Author{}, err
    }

    return auths, nil
}
