package book

import (
    "context"
    "library/internal/entities"
    "database/sql"
    "library/internal/repository"
)

type BookService struct {
    r repository.Repository
}

func NewBookService(db *sql.DB) *BookService {
    return &BookService{
        r: *repository.NewRepository(db),
    }
}

func (b BookService) Create(ctx context.Context, book entities.Book, authorID int) error {
    return b.r.Book.Create(ctx, book, authorID)
}

func (b BookService) GetByID(ctx context.Context, id int) (entities.Book, error) {
    return b.r.Book.GetById(ctx, id)
}

func (b BookService) GetByUserID(ctx context.Context, id int) ([]entities.Book, error) {
    return b.r.Book.GetByUserId(ctx, id)
}

func (b BookService) GetByAuthorsID(ctx context.Context, id int) ([]entities.Book, error) {
    return b.r.Book.GetByAuthorsID(ctx, id)
}

func (b BookService) Exists(ctx context.Context, id int) (bool, error) {
    return b.r.Book.Exists(ctx, id)
}

func (b BookService) CheckRent(ctx context.Context, id int) (bool, error) {
    return b.r.Book.CheckRent(ctx, id)
}

func (b BookService) Rent(ctx context.Context, userId, id int) error {
    return b.r.Book.Rent(ctx, userId, id)
}

func (b BookService) List(ctx context.Context) ([]entities.Book, error) {
    return b.r.Book.List(ctx)
}

func (b BookService) Return(ctx context.Context, userId, id int) error {
    return b.r.Book.Return(ctx, userId, id)
}
