package user

import (
    "library/internal/entities"
    "context"
    "library/internal/repository"
    "database/sql"
)

type UserService struct {
    r repository.Repository
}

func NewUserService(db *sql.DB) *UserService {
    return &UserService{r: *repository.NewRepository(db)}
}

func (u UserService) Create(ctx context.Context, user entities.User) error {
    return u.r.User.Create(ctx, user)
}

func (u UserService) GetByID(ctx context.Context, id int) (entities.User, error) {
    return u.r.User.GetById(ctx, id)
}

func (u UserService) Exists(ctx context.Context, id int) (bool, error) {
    return u.r.User.Exists(ctx, id)
}

func (u UserService) List(ctx context.Context) ([]entities.User, error) {
    return u.r.User.List(ctx)
}
