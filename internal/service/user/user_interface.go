package user

import (
    "context"
    "library/internal/entities"
)

type UserServicer interface {
    Create(ctx context.Context, user entities.User) error

    GetByID(ctx context.Context, id int) (entities.User, error)

    Exists(ctx context.Context, id int) (bool, error)

    List(ctx context.Context) ([]entities.User, error)
}
