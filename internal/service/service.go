package service

import (
    "library/internal/service/author"
    "library/internal/service/book"
    "library/internal/service/user"
    "database/sql"
    "library/internal/entities"
    "context"
    "fmt"
)

type LibraryService struct {
    authorService author.AuthorServicer
    bookService   book.BookServicer
    userService   user.UserServicer
}

type LibraryServicer interface {
    GetBook(bookID int, userID int) (entities.Book, error)
    ReturnBook(bookID int, userID int) error
    GetUsers() ([]entities.User, error)
    GetAuthors() ([]entities.Author, error)
    CreateBook(b entities.Book, authorID int) error
    CreateAuthor(a entities.Author) error
    CreateUser(u entities.User) error
}

type LibraryFacade struct {
    libraryService *LibraryService
}

func NewLibraryFacade(db *sql.DB) *LibraryFacade {
    return &LibraryFacade{
        libraryService: &LibraryService{
            authorService: *author.NewAuthorService(db),
            bookService:   *book.NewBookService(db),
            userService:   *user.NewUserService(db),
        },
    }
}

func (ls *LibraryFacade) GetBook(bookID int, userID int) (entities.Book, error) {
    bookEx, err := ls.libraryService.bookService.Exists(context.Background(), int(bookID))
    if err != nil {
        return entities.Book{}, err
    }
    if !bookEx {
        return entities.Book{}, err
    }

    rent, err := ls.libraryService.bookService.CheckRent(context.Background(), int(bookID))
    if err != nil {
        return entities.Book{}, err
    }
    if rent {
        return entities.Book{}, err
    }

    userEx, err := ls.libraryService.userService.Exists(context.Background(), int(userID))
    if err != nil {
        return entities.Book{}, err
    }
    if !userEx {
        return entities.Book{}, err
    }

    err = ls.libraryService.bookService.Rent(context.Background(), int(userID), int(bookID))
    if err != nil {
        return entities.Book{}, err
    }

    b, err := ls.libraryService.bookService.GetByID(context.Background(), int(bookID))
    if err != nil {
        return entities.Book{}, err
    }

    a, err := ls.libraryService.authorService.GetByID(context.Background(), b.AuthorID)
    if err != nil {
        return entities.Book{}, err
    }
    b.Author = a

    return b, nil
}

func (ls *LibraryFacade) ReturnBook(bookID int, userID int) error {
    bookEx, err := ls.libraryService.bookService.Exists(context.Background(), int(bookID))
    if err != nil {
        fmt.Println("bookEx, err := ls.libraryService.bookService.Exists(context.Background(), int(bookID))")
        return err
    }
    if !bookEx {
        return err
    }

    rent, err := ls.libraryService.bookService.CheckRent(context.Background(), int(bookID))
    if err != nil {
        fmt.Println("rent, err := ls.libraryService.bookService.CheckRent(context.Background(), int(bookID))")
        return err
    }
    if !rent {
        return err
    }

    userEx, err := ls.libraryService.userService.Exists(context.Background(), int(userID))
    if err != nil {
        fmt.Println("userEx, err := ls.libraryService.userService.Exists(context.Background(), int(userID))")
        return err
    }
    if !userEx {
        return err
    }

    return ls.libraryService.bookService.Return(context.Background(), userID, bookID)
}

func (ls *LibraryFacade) GetUsers() ([]entities.User, error) {
    users, err := ls.libraryService.userService.List(context.Background())
    if err != nil {
        fmt.Println("users, err := ls.libraryService.userService.List(context.Background())")
        return []entities.User{}, err
    }

    for i, u := range users {
        bk, err := ls.libraryService.bookService.GetByUserID(context.Background(), u.ID)
        for i2, e := range bk {
            at, err := ls.libraryService.authorService.GetByID(context.Background(), e.AuthorID)
            if err != nil {
                fmt.Println("at,err := ls.libraryService.authorService.GetByID(context.Background(),e.AuthorID)")
                return []entities.User{}, err
            }
            bk[i2].Author = at
        }
        if err != nil {
            fmt.Println("bk, err := ls.libraryService.bookService.GetByUserID(context.Background(), u.ID)")
            return []entities.User{}, err
        }
        users[i].RentedBooks = bk
    }

    return users, nil
}

func (ls *LibraryFacade) GetAuthors() ([]entities.Author, error) {
    authors, err := ls.libraryService.authorService.List(context.Background())
    if err != nil {
        fmt.Println("authors, err := ls.libraryService.authorService.List(context.Background())")
        return []entities.Author{}, err
    }

    for i, a := range authors {
        bk, err := ls.libraryService.bookService.GetByAuthorsID(context.Background(), a.ID)
        if err != nil {
            fmt.Println("bk, err := ls.libraryService.bookService.GetByAuthorsID(context.Background(), a.ID)")
            return []entities.Author{}, err
        }
        authors[i].Books = bk
    }

    return authors, nil
}

func (ls *LibraryFacade) CreateBook(b entities.Book, authorID int) error {
    return ls.libraryService.bookService.Create(context.Background(), b, authorID)
}

func (ls *LibraryFacade) CreateAuthor(a entities.Author) error {
    return ls.libraryService.authorService.Create(context.Background(), a)
}

func (ls *LibraryFacade) CreateUser(u entities.User) error {
    return ls.libraryService.userService.Create(context.Background(), u)
}
