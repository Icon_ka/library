package entities

import "database/sql"

type Author struct {
    ID    int    `json:"id"`
    Name  string `json:"name"`
    Books []Book `json:"books"`
}

type Book struct {
    ID       int           `json:"id"`
    Title    string        `json:"title"`
    Author   Author        `json:"author"`
    AuthorID int           `json:"author_id"`
    Rented   bool          `json:"rented"`
    UserID   sql.NullInt64 `json:"user_id"`
}

type User struct {
    ID          int    `json:"id"`
    Name        string `json:"name"`
    RentedBooks []Book `json:"rented_books"`
}
