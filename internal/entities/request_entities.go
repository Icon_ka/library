package entities

type Request struct {
    BookId int `json:"book_id"`
    UserId int `json:"user_id"`
}
