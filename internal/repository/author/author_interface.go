package author

import (
    "context"
    "library/internal/entities"
)

type AuthorRepositorer interface {
    Create(ctx context.Context, author entities.Author) error

    GetById(ctx context.Context, id int) (entities.Author, error)

    Exists(ctx context.Context, id int) (bool, error)

    List(ctx context.Context) ([]entities.Author, error)
}
