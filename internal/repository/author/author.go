package author

import (
    "library/internal/entities"
    "database/sql"
    "github.com/Masterminds/squirrel"
    "context"
    "log"
)

type AuthorPostgresRepository struct {
    pg      *sql.DB
    builder squirrel.StatementBuilderType
}

func NewAuthorRepository(db *sql.DB) *AuthorPostgresRepository {
    return &AuthorPostgresRepository{
        pg:      db,
        builder: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
    }
}

func (a AuthorPostgresRepository) Create(ctx context.Context, author entities.Author) error {
    query, args, err := a.builder.Insert("authors").
        Columns("name").
        Values(author.Name).
        ToSql()
    if err != nil {
        return err
    }

    _, err = a.pg.ExecContext(ctx, query, args...)
    if err != nil {
        return err
    }

    return nil
}

func (a AuthorPostgresRepository) GetById(ctx context.Context, id int) (entities.Author, error) {
    query, args, err := a.builder.Select("*").
        From("authors").
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return entities.Author{}, err
    }

    var author entities.Author
    err = a.pg.QueryRowContext(ctx, query, args...).Scan(&author.ID, &author.Name)
    if err != nil {
        return entities.Author{}, err
    }

    return author, nil
}

func (a AuthorPostgresRepository) Exists(ctx context.Context, id int) (bool, error) {
    query := `SELECT EXISTS(SELECT id FROM authors WHERE id = $1)`

    var exist bool
    err := a.pg.QueryRowContext(ctx, query, id).Scan(&exist)
    if err != nil {
        return false, err
    }

    return exist, nil
}

func (a AuthorPostgresRepository) List(ctx context.Context) ([]entities.Author, error) {
    query, args, err := a.builder.
        Select("*").From("authors").
        ToSql()
    if err != nil {
        return []entities.Author{}, err
    }

    var authors []entities.Author
    rows, err := a.pg.QueryContext(ctx, query, args...)
    if err != nil {
        return []entities.Author{}, err
    }
    for rows.Next() {
        var author entities.Author
        err = rows.Scan(&author.ID, &author.Name)
        if err != nil {
            return []entities.Author{}, err
        }
        authors = append(authors, author)
    }

    if err = rows.Err(); err != nil {
        return []entities.Author{}, err
    }

    log.Print(authors)
    return authors, nil
}
