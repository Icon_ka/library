package user

import (
    "library/internal/entities"
    "github.com/Masterminds/squirrel"
    "database/sql"
    "context"
)

type UserPostgresRepository struct {
    pg      *sql.DB
    builder squirrel.StatementBuilderType
}

func NewUserRepository(db *sql.DB) *UserPostgresRepository {
    return &UserPostgresRepository{
        pg:      db,
        builder: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
    }
}

func (u UserPostgresRepository) Create(ctx context.Context, user entities.User) error {
    query, args, err := u.builder.Insert("users").
        Columns("name").
        Values(user.Name).
        ToSql()
    if err != nil {
        return err
    }

    _, err = u.pg.ExecContext(ctx, query, args...)
    if err != nil {
        return err
    }

    return nil
}

func (u UserPostgresRepository) GetById(ctx context.Context, id int) (entities.User, error) {
    query, args, err := u.builder.Select("*").
        From("users").
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return entities.User{}, err
    }

    var user entities.User
    err = u.pg.QueryRowContext(ctx, query, args...).Scan(&user.ID, &user.Name)
    if err != nil {
        return entities.User{}, err
    }

    return user, nil
}

func (u UserPostgresRepository) Exists(ctx context.Context, id int) (bool, error) {
    query := `SELECT EXISTS(SELECT id FROM users WHERE id = $1)`

    var exist bool
    err := u.pg.QueryRowContext(ctx, query, id).Scan(&exist)
    if err != nil {
        return false, err
    }

    return exist, nil
}

func (u UserPostgresRepository) List(ctx context.Context) ([]entities.User, error) {
    query, args, err := u.builder.
        Select("*").From("users").
        ToSql()
    if err != nil {
        return []entities.User{}, err
    }

    var users []entities.User
    rows, err := u.pg.QueryContext(ctx, query, args...)
    if err != nil {
        return []entities.User{}, err
    }
    for rows.Next() {
        var user entities.User
        err = rows.Scan(&user.ID, &user.Name)
        if err != nil {
            return []entities.User{}, err
        }
        users = append(users, user)
    }

    if err = rows.Err(); err != nil {
        return []entities.User{}, err
    }

    return users, nil
}
