package user

import (
    "context"
    "library/internal/entities"
)

type UserRepositorer interface {
    Create(ctx context.Context, user entities.User) error

    GetById(ctx context.Context, id int) (entities.User, error)

    Exists(ctx context.Context, id int) (bool, error)

    List(ctx context.Context) ([]entities.User, error)
}
