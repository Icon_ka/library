package book

import (
    "context"
    "library/internal/entities"
    "database/sql"
    "github.com/Masterminds/squirrel"
    "log"
)

type BookPostgreSQL struct {
    pg      *sql.DB
    builder squirrel.StatementBuilderType
}

func NewBookPostgreSQL(db *sql.DB) *BookPostgreSQL {
    return &BookPostgreSQL{
        pg:      db,
        builder: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
    }
}

func (b BookPostgreSQL) Create(ctx context.Context, book entities.Book, authorID int) error {
    query, args, err := b.builder.Insert("books").
        Columns("title", "author_id", "rented").
        Values(book.Title, authorID, false).
        ToSql()
    if err != nil {
        return err
    }

    _, err = b.pg.ExecContext(ctx, query, args...)
    if err != nil {
        return err
    }

    return nil
}

func (b BookPostgreSQL) GetById(ctx context.Context, id int) (entities.Book, error) {
    query, args, err := b.builder.Select("*").
        From("books").
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return entities.Book{}, err
    }

    var book entities.Book
    err = b.pg.QueryRowContext(ctx, query, args...).Scan(&book.ID, &book.Title, &book.AuthorID, &book.Rented, &book.UserID)
    if err != nil {
        return entities.Book{}, err
    }

    return book, nil
}

func (b BookPostgreSQL) GetByUserId(ctx context.Context, id int) ([]entities.Book, error) {
    query, args, err := b.builder.Select("*").
        From("books").
        Where(squirrel.Eq{"user_id": id}).
        ToSql()
    if err != nil {
        return []entities.Book{}, err
    }

    var books []entities.Book
    rows, err := b.pg.QueryContext(ctx, query, args...)
    if err != nil {
        return []entities.Book{}, err
    }
    defer rows.Close()

    for rows.Next() {
        var book entities.Book
        err := rows.Scan(&book.ID, &book.Title, &book.AuthorID, &book.Rented, &book.UserID)
        if err != nil {
            return []entities.Book{}, err
        }
        books = append(books, book)
    }

    return books, nil
}

func (b BookPostgreSQL) GetByAuthorsID(ctx context.Context, id int) ([]entities.Book, error) {
    query, args, err := b.builder.Select("*").
        From("books").
        Where(squirrel.Eq{"author_id": id}).
        ToSql()
    if err != nil {
        return []entities.Book{}, err
    }

    var books []entities.Book
    rows, err := b.pg.QueryContext(ctx, query, args...)
    if err != nil {
        return []entities.Book{}, err
    }
    defer rows.Close()

    for rows.Next() {
        var book entities.Book
        err := rows.Scan(&book.ID, &book.Title, &book.AuthorID, &book.Rented, &book.UserID)
        if err != nil {
            return []entities.Book{}, err
        }
        if book.UserID.Valid {
            book.UserID = sql.NullInt64{Int64: book.UserID.Int64, Valid: true}
        } else {
            book.UserID = sql.NullInt64{Int64: 0, Valid: false}
        }
        books = append(books, book)
    }

    return books, nil
}

func (b BookPostgreSQL) Exists(ctx context.Context, id int) (bool, error) {
    query := `SELECT EXISTS(SELECT id FROM books WHERE id = $1)`

    var exist bool
    err := b.pg.QueryRowContext(ctx, query, id).Scan(&exist)
    if err != nil {
        return false, err
    }

    return exist, nil
}

func (b BookPostgreSQL) CheckRent(ctx context.Context, id int) (bool, error) {
    query, args, err := b.builder.Select("rented").
        From("books").
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return false, err
    }

    var rented bool
    err = b.pg.QueryRowContext(ctx, query, args...).Scan(&rented)
    if err != nil {
        return false, err
    }

    return rented, nil
}

func (b BookPostgreSQL) Rent(ctx context.Context, userId, id int) error {
    query, args, err := b.builder.
        Update("books").
        Set("rented", true).
        Set("user_id", userId).
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return err
    }
    _, err = b.pg.ExecContext(ctx, query, args...)
    if err != nil {
        return err
    }

    return nil
}

func (b BookPostgreSQL) List(ctx context.Context) ([]entities.Book, error) {
    query, args, err := b.builder.
        Select("*").From("books").
        ToSql()
    if err != nil {
        return []entities.Book{}, err
    }

    var books []entities.Book
    rows, err := b.pg.QueryContext(ctx, query, args...)
    if err != nil {
        return []entities.Book{}, err
    }
    for rows.Next() {
        var book entities.Book
        err = rows.Scan(&book.ID, &book.Title, &book.Author.ID, &book.Rented, &book.UserID)
        if err != nil {
            return []entities.Book{}, err
        }
        books = append(books, book)
    }

    if err = rows.Err(); err != nil {
        return []entities.Book{}, err
    }

    log.Print(books)

    return books, nil
}

func (b BookPostgreSQL) Return(ctx context.Context, userId, id int) error {
    query, args, err := b.builder.
        Update("books").
        Set("rented", false).
        Set("user_id", 0).
        Where(squirrel.Eq{"id": id}).
        ToSql()
    if err != nil {
        return err
    }
    _, err = b.pg.ExecContext(ctx, query, args...)
    if err != nil {
        return err
    }

    return nil
}
