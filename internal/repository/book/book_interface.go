package book

import (
    "context"
    "library/internal/entities"
)

type BookRepositorer interface {
    Create(ctx context.Context, book entities.Book, authorID int) error

    GetById(ctx context.Context, id int) (entities.Book, error)

    GetByAuthorsID(ctx context.Context, id int) ([]entities.Book, error)

    Exists(ctx context.Context, id int) (bool, error)

    CheckRent(ctx context.Context, id int) (bool, error)

    Rent(ctx context.Context, userId, id int) error

    List(ctx context.Context) ([]entities.Book, error)

    Return(ctx context.Context, userId, id int) error

    GetByUserId(ctx context.Context, id int) ([]entities.Book, error)
}
