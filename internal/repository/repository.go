package repository

import (
    "library/internal/repository/user"
    "library/internal/repository/book"
    "library/internal/repository/author"
    "database/sql"
)

type Repository struct {
    User   user.UserRepositorer
    Book   book.BookRepositorer
    Author author.AuthorRepositorer
}

func NewRepository(db *sql.DB) *Repository {
    return &Repository{
        User:   user.NewUserRepository(db),
        Book:   book.NewBookPostgreSQL(db),
        Author: author.NewAuthorRepository(db),
    }
}
