package library

import "net/http"

type Librarer interface {
    GetBook(http.ResponseWriter, *http.Request)
    ReturnBook(http.ResponseWriter, *http.Request)
    GetUsers(http.ResponseWriter, *http.Request)
    GetAuthors(http.ResponseWriter, *http.Request)
    CreateBook(http.ResponseWriter, *http.Request)
    CreateAuthor(http.ResponseWriter, *http.Request)
}
