package library

import (
    "net/http"
    "library/internal/service"
    "database/sql"
    "library/internal/entities"
    "encoding/json"
    "fmt"
)

type LibraryController struct {
    service service.LibraryServicer
}

func NewLibraryController(db *sql.DB) *LibraryController {
    return &LibraryController{
        service: service.NewLibraryFacade(db),
    }
}

func (l LibraryController) GetBook(writer http.ResponseWriter, request *http.Request) {
    var req entities.Request
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при декодировании JSON")
        return
    }

    book, err := l.service.GetBook(req.BookId, req.UserId)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных", err)
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, book)
}

func (l LibraryController) ReturnBook(writer http.ResponseWriter, request *http.Request) {
    var req entities.Request
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при декодировании JSON", err)
        return
    }

    err = l.service.ReturnBook(req.BookId, req.UserId)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных")
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, "Книга сдана")
}

func (l LibraryController) GetUsers(writer http.ResponseWriter, request *http.Request) {
    users, err := l.service.GetUsers()
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных", err)
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, users)
}

func (l LibraryController) GetAuthors(writer http.ResponseWriter, request *http.Request) {
    authors, err := l.service.GetAuthors()
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных: ", err)
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, authors)
}

func (l LibraryController) CreateBook(writer http.ResponseWriter, request *http.Request) {
    var req entities.Book
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при декодировании JSON")
        return
    }

    err = l.service.CreateBook(req, req.AuthorID)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных", err)
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, "Книга создана")
}

func (l LibraryController) CreateAuthor(writer http.ResponseWriter, request *http.Request) {
    var req entities.Author
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при декодировании JSON")
        return
    }
    err = l.service.CreateAuthor(req)

    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        fmt.Fprint(writer, "Ошибка при обращении к базе данных", err)
        return
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, "Автор создан")
}
