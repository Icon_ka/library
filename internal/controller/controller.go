package controller

import (
    "library/internal/controller/library"
    "database/sql"
)

type Controller struct {
    Library library.Librarer
}

func NewController(db *sql.DB) *Controller {
    return &Controller{Library: library.NewLibraryController(db)}
}
