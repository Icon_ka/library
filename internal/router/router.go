package router

import (
    "github.com/go-chi/chi"

    "net/http"
    "go.uber.org/zap"
    "library/internal/controller"
)

func NewApiRouter(controllers *controller.Controller) http.Handler {
    r := chi.NewRouter()

    r.Use(LoggerMiddleware)

    r.Post("/getBook", controllers.Library.GetBook)
    r.Post("/returnBook", controllers.Library.ReturnBook)
    r.Get("/getUsers", controllers.Library.GetUsers)
    r.Get("/getAuthors", controllers.Library.GetAuthors)
    r.Post("/createBook", controllers.Library.CreateBook)
    r.Post("/createAuthor", controllers.Library.CreateAuthor)

    return r
}

func LoggerMiddleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        logger, _ := zap.NewProduction()
        defer logger.Sync()

        logger.Info("Request received",
            zap.String("path", r.URL.Path),
            zap.String("method", r.Method),
            zap.String("ip", r.RemoteAddr),
        )

        next.ServeHTTP(w, r)
    })
}
