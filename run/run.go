package run

import (
    "os"
    "library/internal/repository"
    "net/http"
    "library/internal/service"
    "log"
    "time"
    "syscall"
    "os/signal"
    "context"
    "fmt"
    "github.com/joho/godotenv"
    "github.com/golang-migrate/migrate/v4"
    "github.com/golang-migrate/migrate/v4/database/postgres"
    "database/sql"
    "library/internal/router"
    "library/internal/controller"
    "library/internal/entities"
    "github.com/brianvoe/gofakeit/v6"
)

// Application - интерфейс приложения
type Application interface {
    Runner
    Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
    Run()
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
    Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
    srv      *http.Server
    Repo     *repository.Repository
    Services *service.LibraryFacade
}

func NewApp() *App {
    return &App{}
}

// Run - запуск приложения
func (a *App) Run() {
    go func() {
        log.Println("Starting server...")
        if err := a.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
            log.Fatalf("Server error: %v", err)
        }
    }()

    sigChan := make(chan os.Signal, 1)
    signal.Notify(sigChan, syscall.SIGINT)

    <-sigChan

    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    err := a.srv.Shutdown(ctx)
    if err != nil {
        log.Fatalf("Server shutdown error: %v", err)
    }

    <-ctx.Done()

    log.Println("Server stopped gracefully")
}

func (a *App) Bootstrap(options ...interface{}) Runner {
    err := godotenv.Load(".env")
    if err != nil {
        log.Fatal("Ошибка при загрузке файла .env")
    }

    db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
        os.Getenv("DB_USER"),
        os.Getenv("DB_PASSWORD"),
        os.Getenv("DB_HOST"),
        os.Getenv("DB_PORT"),
        os.Getenv("DB_NAME"),
    ))
    if err != nil {
        log.Fatal(err)
    }

    driver, err := postgres.WithInstance(db, &postgres.Config{})
    if err != nil {
        log.Fatal(err)
    }

    m, err := migrate.NewWithDatabaseInstance(
        "file:///internal/migrations",
        "postgres",
        driver,
    )
    if err != nil {
        log.Fatal(err)
    }

    err = m.Up()
    if err != nil {
        log.Fatal(err)
    }

    s := service.NewLibraryFacade(db)

    for i := 0; i < 10; i++ {
        err = s.CreateAuthor(entities.Author{
            Name:  gofakeit.Name(),
            Books: nil,
        })
        if err != nil {
            return nil
        }
    }

    for i := 0; i < 100; i++ {
        err = s.CreateBook(entities.Book{
            ID:    0,
            Title: gofakeit.BookTitle(),
        }, gofakeit.IntRange(1, 10))
        if err != nil {
            return nil
        }
    }

    for i := 0; i < 50; i++ {
        err = s.CreateUser(entities.User{
            ID:   0,
            Name: gofakeit.Name(),
        })
        if err != nil {
            return nil
        }
    }

    ctrl := controller.NewController(db)
    r := router.NewApiRouter(ctrl)
    server := &http.Server{
        Addr:         ":8080",
        Handler:      r,
        ReadTimeout:  10 * time.Second,
        WriteTimeout: 10 * time.Second,
    }
    a.srv = server

    return a
}
